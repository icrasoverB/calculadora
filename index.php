<html>
    <head>
        <title>Calculadora</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    		<meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.number.min.js"></script>
        <script src="https://use.fontawesome.com/92fd17c777.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Dosis|Roboto:300,400" rel="stylesheet">
        <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
        <script>
            $(document).ready(function () {
                //Variable que resibe el text del select de tipo de envio
                var textoTipoEnvio = "";
                //Variable que recibe el text del select de la provincia
                var textoProvincia = "";
                //Llamamos todos los tipos de envios
                $.getJSON('ajax.php?todos_tipo_envio=1', function (dataTipoEnvio) {
                    //Hacemos un bluche por cada tipo de envio y creamos un option
                    $.each(dataTipoEnvio, function (id, valueTipoEnvio) {
                        $("#tipo_envio").append('<option>' + valueTipoEnvio + '</option>');
                    });
                });
                //Si se selecciona un tipo de envio se muestran todas sus provincias
                $("#tipo_envio").change(function () {
                    $(".error").fadeOut(500);
                    $("#precio").text("0");
                    //Sacamos el texto del tipo de envio
                    textoTipoEnvio = $(this).find('option:selected').text();
                    //Si el texto del tipo de envio es diferenete al primer option podemos buscar sus provincias
                    if (textoTipoEnvio !== "Seleccione un tipo de envio") {
                        //Quitamos todos los options del select de la provincia
                        $("#provincia").empty();
                        $("#provincia").append('<option>Seleccione una provincia</option>');
                        $.getJSON('ajax.php?tipo_envio=' + textoTipoEnvio, function (dataProvincia) {
                            //Hacemos un bluche por cada tipo de envio y creamos un option
                            $.each(dataProvincia, function (id, valueProvincia) {
                                $("#provincia").append('<option>' + valueProvincia + '</option>');
                            });
                        });
                    }
                    //Se cambia el select de la provincia hacia el primer option
                    else {
                        $("#provincia option:contains(Seleccione una provincia)").attr("selected", true);
                        $("#provincia").change();
                        $("#provincia").empty();
                        $("#provincia").append('<option>Seleccione una provincia</option>');

                    }
                });
                //Si se selecciona una provincia se  muestra el input para poner el peso
                $("#provincia").change(function () {
                    //El precio se vacia
                    $("#precio").text("0");
                    $(".error").fadeOut(500);
                    //Sacamos el texto del select de la provincia
                    textoProvincia = $(this).find('option:selected').text();
                    if (textoProvincia !== "Seleccione una provincia") {
                        $("#peso").prop('disabled', false);
                        $("#calcular").prop('disabled', false);
                    }
                    //Si no se ha seleccionado ninguna provincia
                    else {
                        //El input peso se pone vacio
                        $("#peso").val("0");
                        $(".error").fadeOut(500);
                        //El input peso se deshabilita
                        $("#peso").prop('disabled', true);
                        //El boton calcular tambien se deshabilita
                        $("#calcular").prop('disabled', true);
                    }
                });
                //Al hacer enter en el input text peso gatilla al boton calcular
                $("#peso").keyup(function (e) {
                    if (e.keyCode == 13){
                        $("#calcular").click();
                    }
                });
                //Si se da click en calcular
                $("#calcular").click(function () {

                    $('#calcular i').show();
                    //consultamos el ajax y pasamos los parametros de tipo envio,provincia y peso para resivir de regreso el precio
                    $.getJSON('ajax.php?tipo_envio=' + textoTipoEnvio + '&provincia=' + textoProvincia + '&peso=' + $("#peso").val(), function (dataPrecio) {
                        dataPrecio = $.number(dataPrecio, 2);
                        $("#precio").fadeIn(500).text(dataPrecio);
                        $('#calcular i').hide();
                        $(".error").fadeOut(500);
                    }).fail(function (d) {
                        $(".error").fadeIn(500);
                        $("#peso").val("0");
                        $("#precio").text("0");
                        $(".error .msj").text("Rango de peso no encontrado porfavor intentelo de nuevo");
                        $('#calcular i').hide();
                    });
                });

            });
        </script>
    </head>
    <body>

      <div class="calculadora">

        <div class="contenido">
          <div class="titulo_top">
              Calculo de libras para mpresas asociadas.
          </div>
          <div>
              <select id="tipo_envio" class="form-control">
                  <option class="color">Seleccione un tipo de envio</option>
              </select>
          </div>
          <div>
              <select id="provincia" class="form-control">
                  <option class="color">Seleccione una provincia</option>
              </select>
          </div>
          <div>
              <input type="text" id="peso"  placeholder="Peso en Lbs" disabled="disabled" class="form-control">
          </div>
          <div>
              <button type="button" id="calcular" disabled="disabled" class="btn btn-danger form-control"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
 CALCULAR ENVÍO</button>
          </div>
        </div>

      <div class="resultado_content">
        <div class="resultado">
            <span class="moneda">$</span>
            <span id="precio">0,00</span>
        </div>
        <div class="info">
            * Coste de envio. No incluye arangel aduanal, ni deducibles
        </div>
      </div>

        <br>
        <div class="titulo_botton">
            Esta entrega tiene un plazo estimado de <span class="dias">XX</span> dias. Puede consultar los datos del seguimiento en <a href="#">www.paginaweb.com</a>
        </div>


        <div class="error">
          <span class="msj"></span>
        </div>

      </div>
    </body>
</html>
