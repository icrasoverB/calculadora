<?php
/*
 * Este archivo se encarga de jalar los archivos excel que estan en la carpeta /tipos_envio/
 * El nombre de esos archivos es tomado como tipo de envio
 * Las hojas de cada uno de los archivos es tomado como provincia
 * El contenido de las hojas es tomado como rango de pesos y el precio de ese rango de precio
 * Este script resibe 3 tipos de parametros via $_GET
 *      $_GET["todos_tipo_envio"] == "1"                                                    entrega los nombres de los archivos en la carpeta /tipos_envio/
 *      isset($_GET["tipo_envio"]) && !isset($_GET["provincia"])                            imprime todas las provincias
 *      isset($_GET["tipo_envio"]) && isset($_GET["provincia"]) && isset($_GET["peso"])     imprime el precio dependiendo del peso
 * 
*/
//ini_set('display_errors', 'On');
//Clases necesarias
require_once('Classes/PHPExcel.php');
require_once('Classes/PHPExcel/Reader/Excel2007.php');
// Cargando la hoja de cálculo
$objReader = new PHPExcel_Reader_Excel2007();
//Seleccionamos la carpeta en la que estan nuestros xlsx
$directorio = opendir("tipos_envio");
//Bucle que saca cada archivo
while ($archivo = readdir($directorio)) {
    //verificamos que no sea carpeta
    if (!is_dir($archivo)) {
        //Guardamos los nombres de los excels
        $nombreExcel = $archivo;
        //Guardamos solo el nombre (sin .xlsx) para mostrar como tipo de envio
        $nombreTipoEnvio = str_replace(".xlsx", "", $archivo);
        //Leemos el archivo (tipo envio)
        $objPHPExcel = $objReader->load("tipos_envio/" . $nombreExcel);
        //Sacamos el numero de hojas en el excel
        $numeroHojas = $objPHPExcel->getSheetCount();

        //Bucle que pasa por cada una de las hojas del excel
        for ($numHoja = 0; $numHoja < $numeroHojas; $numHoja++) {
            //Activamos cada hoja
            $objPHPExcel->setActiveSheetIndex($numHoja);
            //Sacamos el nombre de la hoja activa
            $nombreHoja = $objPHPExcel->getActiveSheet()->getTitle();
            //Tomamos el numero total de registros de la hoja activa
            $numTotalRegistros = $objPHPExcel->getActiveSheet()->getHighestRow();
            //Hacemos un bucle desde el registro 3 hasta el ultimo registro de la hoja activa
            for ($registro = 3; $registro <= $numTotalRegistros; $registro++) {
                //Matris que almacena el rango de libras inicial, asi= [nombre tipo envio(string)][Nombre de hoja (string)][Rango Inicial de libra (int)]
                $rangoIniLibras[$nombreTipoEnvio][$nombreHoja][] = $objPHPExcel->getActiveSheet()->getCell('A' . $registro)->getCalculatedValue();
                //Matris que almacena el rango de libras inicial, asi= [nombre tipo envio(string)][Nombre de hoja (string)][Rango Final de libra (int)]
                $rangoFinLibras[$nombreTipoEnvio][$nombreHoja][] = $objPHPExcel->getActiveSheet()->getCell('B' . $registro)->getCalculatedValue();
                //Matris que almacena el el precio por rango de libras, asi= [nombre tipo envio(string)][Nombre de hoja (string)][Rango Final de libra (int)]
                $precio[$nombreTipoEnvio][$nombreHoja][] = $objPHPExcel->getActiveSheet()->getCell('C' . $registro)->getCalculatedValue();
            }
        }
    }
}
//Si si se manda por GET todos los tipos de envio se imprimen todos los tipos de envio
if ($_GET["todos_tipo_envio"] == "1") {
    echo json_encode(array_keys($rangoIniLibras));
}
//Si se manda solo el tipo de envio (sin provincia) se imprimen todas sus provincias
if (isset($_GET["tipo_envio"]) && !isset($_GET["provincia"])) {
    echo json_encode(array_keys($rangoIniLibras[$_GET["tipo_envio"]]));
}
//Si se manda el tipo de envio, las provincia y el peso se imprime el precio
if (isset($_GET["tipo_envio"]) && isset($_GET["provincia"]) && isset($_GET["peso"])) {
    $peso = $_GET["peso"];
    //Sacamos el numero de rango de libras
    $numeroRangoLibras = count($rangoIniLibras[$_GET["tipo_envio"]][$_GET["provincia"]]);
    //Variable que funciona como bandera para saber si el rango entro en el peso que se recibe
    $entro=0;
    //Bucle que hace pasar todos los rango de este tipo de envio y provincia
    for ($registroRango = 0; $registroRango < $numeroRangoLibras; $registroRango++) {
        $rangoInicial = $rangoIniLibras[$_GET["tipo_envio"]][$_GET["provincia"]][$registroRango];
        $rangoFinal = $rangoFinLibras[$_GET["tipo_envio"]][$_GET["provincia"]][$registroRango];
        //Si el peso que se esta recibiendo es mayor al rango inicial y menor o igua al rango final se toma este precio
        if ($peso > $rangoInicial && $peso <= $rangoFinal) {
            echo $precio[$_GET["tipo_envio"]][$_GET["provincia"]][$registroRango];
            $entro =1;
        }
    }
    //Si no encontro ningun rango
    if($entro==0){
        echo 'e:100';
    }
}
?>
